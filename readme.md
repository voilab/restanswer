# REST Answer

Response system for REST Apis.
Provide standard ways to handle rest responses.

## How to Install

#### using [Composer](http://getcomposer.org/)

Create a composer.json file in your project root:

```json
{
    "require": {
        "voilab/restanswer": "0.1.*"
    }
}
```

Then run the following composer command:

```bash
$ php composer.phar install
```

#### Usage

...see the code...

## Authors

[Joel Poulin](http://www.voilab.org)

## License

MIT Public License
