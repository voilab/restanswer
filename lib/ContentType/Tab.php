<?php
namespace Voilab\Restanswer\ContentType;

use Voilab\Restanswer\Renderer;

class Tab extends Separated {
    public $separator = "\t";
}