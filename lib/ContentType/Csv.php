<?php
namespace Voilab\Restanswer\ContentType;

use Voilab\Restanswer\Renderer;

class Csv extends Separated {
    public $separator = ';';
}